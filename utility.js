function AscendingNumberSort (a,b) {return a-b;}
function DescendingNumberSort(a,b) {return b-a;}

Array.prototype.min = function(){
    return Math.min.apply( Math, this );
};

Array.prototype.max = function(){
    return Math.max.apply( Math, this );
};

Array.prototype.contains = function(obj) {
    var index = this.indexOf(obj);
    return (-1 < index);
};

Array.prototype.sortAscending = function() {
    this.sort(AscendingNumberSort);
}

Array.prototype.sortDescending = function() {
    this.sort(DescendingNumberSort);
}

if (!Array.prototype.filter)
{
   Array.prototype.filter = function(fun /*, thisp*/)
   {
      var len = this.length;
      if (typeof fun != "function")
      throw new TypeError();
      
      var res = new Array();
      var thisp = arguments[1];
      for (var i = 0; i < len; i++)
      {
         if (i in this)
         {
            var val = this[i]; // in case fun mutates this
            if (fun.call(thisp, val, i, this))
            res.push(val);
         }
      }
      return res;
   };
}

if (!Array.prototype.map)
{
   Array.prototype.map = function(fun /*, thisp*/)
   {
      var len = this.length;
      
      if (typeof fun != "function")
      throw new TypeError();
      
      var res = new Array(len);
      var thisp = arguments[1];
      
      for (var i = 0; i < len; i++)
      {
         if (i in this)
         res[i] = fun.call(thisp, this[i], i, this);
      }
      return res;
   };
}

d3.selection.prototype.moveToFront = function() {  
  return this.each(function(){
    var pN = this.parentNode;
    pN.removeChild(this);
    pN.appendChild(this);
  });
};

d3.selection.prototype.moveToBack = function() {  
    return this.each(function() { 
        var firstChild = this.parentNode.firstChild; 
        if (firstChild) { 
            this.parentNode.insertBefore(this, firstChild); 
        } 
    });
};
