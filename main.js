const data = {
    // axis
    years : [],
    target_countries: [],
    tcnames: [],
    source_countries: [],
 
    // data arrays
    gdp: [],            //idx0: target_countries, idx1: year 
    er: [],             //idx0: target_countries, idx1: year
    migration: [],      //idx0: target_countries, idx1: year, idx2: sourcecountry

    // geographical data
    geodata: []         //idx0: target_countries
};

const geodata = {
    //index names
    countries: [], //country codes
    data: [] //idx0: countries
};

const uidef = {
    color: {
        gdp: '#ffff55',
        er: '#59ff59',
        migration: '#3333EE' 
    }
}

const uistate = {
    target_country: null,
    source_country: null,
    hover_country: null,
    year_start: null,
    year_end: null,
    year_selection: false,
    real_value_chart: null,

    //target country selector visibility
    visibility: [
        false, //geomap
        true   //parallel axis
    ],
    
    //some loading states
    loaded : {
        geo: false,
        migration: false,
        gdp_er: false,
        
        finished: function () {
            return (this.geo && this.migration && this.gdp_er);
        }
    }
};

const update_timeline = createTimeline("#timeline");
const update_parallelaxis = createParallelAxis("#parallelaxis");
const update_geomap = createGeomap("#geographicalmap");
const update_geolist = createGeolist("#geolist");
const update_migrationtoplist = createMigrationTopList("#migrationtopchart");
const update_updateGdpErMigChart = createGdpErMigChart("#gdpermigchart")
const update_chartLegend = createChartLegend("#chartlegend");

d3.json("eu.topojson", function(data) {
    var datafeatures = topojson.feature(data, data.objects.europe).features;
    datafeatures.forEach(function(feature) {
        var cidx = geodata.countries.indexOf(feature.properties.iso_a3);
        if(0 > cidx) {
            cidx = geodata.countries.push(feature.properties.iso_a3) - 1;   
        }
        geodata.data[cidx] = feature;
    });
    uistate.loaded.geo = true;
    initializeUI();
});

d3.csv("./data/gdp_er.csv", function(error, _loaded) {
    if(null != error) {
        console.log(error);
        return;
    }
    
    _loaded.forEach(function(row){
        //insert year and targetcountry and the indices
        var yidx = data.years.indexOf(parseInt(row.year));
        if(0 > yidx) {
            yidx = data.years.push(parseInt(row.year)) - 1;
        }
        var cidx = data.target_countries.indexOf(row.country);
        if(0 > cidx) {
            cidx = data.target_countries.push(row.country) - 1;
        }
        
        //insert gdp and er into their respective arrays
        if(!Array.isArray(data.gdp[cidx])) {
            data.gdp[cidx] = [];
        }
        data.gdp[cidx][yidx] = parseFloat(row.gdp);
        
        if(!Array.isArray(data.er[cidx])) {
            data.er[cidx] = [];
        }
        data.er[cidx][yidx] = parseFloat(row.er);
    });
    uistate.loaded.gdp_er = true;  
    initializeUI();
})

d3.csv("./data/migration.csv", function(error, _loaded) {
    if(null != error) {
        console.log(error);
        return;
    }
    
    _loaded.forEach(function(row) {        
        //insert year and targetcountry and the indices
        var yidx = data.years.indexOf(parseInt(row.year));
        if(0 > yidx) {
            yidx = data.years.push(parseInt(row.year)) - 1;
        }
        var tcidx = data.target_countries.indexOf(row.targetcountry);
        if(0 > tcidx) {
            tcidx = data.target_countries.push(row.targetcountry) - 1;
        }
        var scidx = data.source_countries.indexOf(row.sourcecountry);
        if(0 > scidx) {
            scidx = data.source_countries.push(row.sourcecountry) - 1;
        }
        
        //fill migration data array
        if(!Array.isArray(data.migration[tcidx])) {
            data.migration[tcidx] = [];
        }
        if(!Array.isArray(data.migration[tcidx][yidx])) {
            data.migration[tcidx][yidx] = [];
        }        
        data.migration[tcidx][yidx][scidx] = parseFloat(row.migrationrate);        
    });
  
    uistate.loaded.migration = true;  
    initializeUI();
});


function initializeUI() {
    if(uistate.loaded.finished()) {
        if(0 == data.geodata.length) { // copy geographical data to the correct indices in the main data field
            geodata.countries.forEach(function(ctry, gcidx) {
                var cidx = data.target_countries.indexOf(ctry);
                if(0 <= cidx) {
                    data.geodata.push(geodata.data[gcidx]);
                }
            });            
        }
        
        var yborders = d3.extent(data.years);
        if(null == uistate.year_start) {
            uistate.year_start = yborders[0];
        }
        if(null == uistate.year_end) {
            uistate.year_end = yborders[1];
        }
        updateUI();
    }
}

function updateUI() {    
    updateTargetCountryChartSelectors();
    update_timeline();
    update_geomap();
    update_geolist();
    update_parallelaxis();
    update_migrationtoplist();
    update_updateGdpErMigChart();
    update_chartLegend();
}

function updateTargetCountryChartSelectors() {
    var selectorsTitles = ["Geographical Map Position", "Parallel Axis Plot"];
    var visibility_switches = d3.select("#boxtitle").selectAll('div.geoselector').data(selectorsTitles);
    visibility_switches.enter().append('div')
        .classed('geoselector', true)
        .text(function(id) { return id; });
        
    visibility_switches
        .classed('selected', function(text, idx) {
            return uistate.visibility[idx];
        })     
        .on({
        click: function(id, idx) {
            uistate.visibility.forEach(function(elem, elemidx) {
                uistate.visibility[elemidx] = false;
            });
            uistate.visibility[idx] = !uistate.visibility[idx];
            updateUI();
        }
        });       
    
    visibility_switches.exit().remove();
}

function createChartLegend(chartlegend_id) {
    dim = {
        width: 350,
        height: 128,
        margin_top: 20,
        margin_bottom: 0,
        margin_left: 0,
        margin_right: 100
    };
    
    const container = d3.select(chartlegend_id);
    
    const svg = container.append('svg')
        .attr({
           width: dim.width + dim.margin_left + dim.margin_right,
           height: dim.height + dim.margin_top + dim.margin_bottom
        });
    
    svg.append('rect') //background for real value chart deselection
        .attr('opacity', 0)
        .attr('transform', 'translate(0,0)')
        .attr('width', dim.margin_right + dim.width + dim.margin_left)
        .attr('height', dim.margin_top + dim.height + dim.margin_bottom)
        .on({
            click: function(d) {
                uistate.real_value_chart = null;
                updateUI();
            }
        });
    
    const root = svg.append('g')
        .classed('legend', true)
        .attr('transform', 'translate(' + dim.margin_right + ',' + dim.margin_top + ')');
    
    const legend = d3.legend.color()
            .shape("rect")
            .shapePadding(10)
            .shapeWidth(80)
            .shapeHeight(20)
            .orient("vertical")
            .labelAlign("start")
            .useClass(false)
            .on('cellclick', function(d) {
                uistate.real_value_chart = d;
                updateUI();
            });
    
    const linecolorscale = d3.scale.ordinal()
        .domain(["Gross Domestic Product", "Employment Rate", "Migration"])
        .range([uidef.color.gdp, uidef.color.er, uidef.color.migration]);
    
    const units = ["[MIO €]", " [10e+3 People]", "[People]"];
    
    function update() {        
        const legendgroup = root.call(legend.scale(linecolorscale));
        
        // Allow real valued chart selection by clicking on the color bar
        const swatches = legendgroup.selectAll('rect.swatch');
        swatches.classed('selected', function(sw, swidx) {
            return sw == uistate.real_value_chart;
        });
        
        const labels = legendgroup.selectAll('text.label');
        labels.text(function(label, labelidx) {
            var retval = [label];
            if(2 == labelidx) {
                // Change migration data label to describe the current migration source
                // and migration target country selection
                if(null != uistate.source_country) {
                    retval.push("from " + uistate.source_country);
                }            
                if(null != uistate.target_country) {
                    retval.push("to " + uistate.target_country);
                }
            }
            retval.push(units[labelidx]); // append correct unit of measurement
            return retval.join(" ");
        });
        
    }
    
    return update;
}

function createGdpErMigChart(gdpermigchart_id) {
    const dim = {
        width: 545,
        height: 330,
        margin_top: 25,
        margin_bottom: 25,
        margin_left: 50,
        margin_right: 70
    };
    const def = {
        transition_duration: 1000,
        dotsize: 5  //size of the data point circles
    };
    
    const chart = d3.select(gdpermigchart_id);
    
    const svg = chart.append('svg')
        .attr({
           width: dim.width + dim.margin_left + dim.margin_right,
           height: dim.height + dim.margin_top + dim.margin_bottom
        });
        
    svg.append('rect') //background for real value chart deselection
        .attr('opacity', 0)
        .attr('transform', 'translate(0,0)')
        .attr('width', dim.margin_right + dim.width + dim.margin_left)
        .attr('height', dim.margin_top + dim.height + dim.margin_bottom)
        .on({
            click: function(d) {
                uistate.real_value_chart = null;
                updateUI();
            }
        });
        
    const root = svg.append('g').attr('transform','translate(' + dim.margin_left + ',' + dim.margin_top + ')');
   
    root.append('g').attr('class', 'axis xaxis').attr('transform', 'translate(0,' + dim.height + ')');
    root.append('g').attr('class', 'axis yaxis');
    root.append('g').attr('class', 'axis yaxisreal').attr('transform', 'translate(' + dim.width + ',0)');
    root.append('g').attr('class', 'chart');    

    const yscale = d3.scale.linear().range([dim.height,0]);
    const yscalereal = d3.scale.linear().range([dim.height,0]);
 
    const yaxis = d3.svg.axis().scale(yscale).orient('left'); 
    const yaxisreal = d3.svg.axis().scale(yscalereal).orient('right')
        .tickFormat(d3.format(".2e"));    

    function update() {
        if(!uistate.loaded.finished()) { // wait for all asynchronous data loading to be finished
            return;
        }
        if(uistate.year_start == uistate.year_end && false == uistate.year_selection) { //hide chart if only one year has been selected
            chart.style('display', 'none');
            return;
        }
        else { // show chart if a time interval has been selected
            chart.style('display', 'block');
        }
        
        // Get year names and their corresponding indices for the data fields
        const chart_years = data.years.filter(function(y) {
            var chart_year_start = (null == uistate.year_start ? d3.extent(data.years)[0] : uistate.year_start);
            var chart_year_end = (null == uistate.year_end ? d3.extent(data.years)[1] : uistate.year_end);

            return (chart_year_start <= y &&  y <= chart_year_end);
        });
        const chart_year_idx = chart_years.map(function(d) {
            return data.years.indexOf(d);
        });
        
        const chartdata = []; //chart data object, contains subobjects for gdp, er and migration data

        // get migration target country name and it's index in the data fields
        var chart_target_countries = [uistate.target_country];
        if(null == uistate.target_country) {  // if no target country is selected, use all possible target countries
            chart_target_countries = data.target_countries;
        }
        var chart_target_country_idx = chart_target_countries.map(function(d) {
            return data.target_countries.indexOf(d);
        });

        // get migration source country name and it's index in the data fields
        var chart_source_countries = [uistate.source_country];
        if(null == uistate.source_country) {  // if no source country is selected, use all possible source countries
            chart_source_countries = data.source_countries;
        }
        var chart_source_country_idx = chart_source_countries.map( function(d) {
            return data.source_countries.indexOf(d);
        });

        // gather GDP chart data by summing out over the selected time interval for the 
        // selected migration target country
        var gdpmax = 0; //maximum gdp value in the chart, used for normalization
        var gdpdataset = chart_year_idx.map(function(yidx) {
            var yname = data.years[yidx];
            var value = NaN;
            chart_target_country_idx.forEach(function(tcidx) {
                if(!isNaN(data.gdp[tcidx][yidx])) {
                    if(isNaN(value)) {
                        value = 0;
                    }

                    value += data.gdp[tcidx][yidx];
                }
            });            
            if(!isNaN(value)) {
                if(gdpmax < value) {
                    gdpmax = value;
                }
                return {
                    year: yname,
                    value: value,
                    percentage: null
                };
            }
        });        
        
        gdpdataset = gdpdataset.filter(function(elem) { // filter out non existing elements
            return !!elem;
        });

        gdpdataset.forEach(function(elem) { // normalize values to maximum
            elem.percentage = (0 == gdpmax ? 0 : elem.value/gdpmax);
        }); 
        
        if(0 < gdpdataset.length) {
            chartdata.push({
                name: "Gross Domestic Product",
                data: gdpdataset
            });
        }

        // gather employment rate chart data by summing out over the selected time interval for the 
        // selected migration target country
        var ermax = 0; //maximum er value in the chart, used for normalization
        var erdataset = chart_year_idx.map(function(yidx) {
            var yname = data.years[yidx];
            var value = NaN;
            chart_target_country_idx.forEach(function(tcidx) {
                if(!isNaN(data.er[tcidx][yidx])) {
                    if(isNaN(value)) {
                        value = 0;
                    }
                    value += data.er[tcidx][yidx];
                }
            }); 
            if(!isNaN(value)) {          
                if(ermax < value) {
                    ermax = value;
                }
                return {
                    year: yname,
                    value: value,
                    percentage: null
                };
            } 
        }); 
        
        erdataset = erdataset.filter(function(elem) { // filter out non existing elements
            return !!elem;
        });

        erdataset.forEach(function(elem) { // normalize values to maximum
            elem.percentage = (0 == ermax ? 0 : elem.value/ermax);
        });
        
        if(0 < erdataset.length) { 
            chartdata.push({
                name: "Employment Rate",
                data: erdataset
            });
        }
        
        // gather migration chart data by summing outover the selected time interval for the 
        // selected migration target country and the selected migration source country
        var migmax = 0; //maximum migration rate value in the chart, used for normalization
        var migdataset = chart_year_idx.map(function(yidx) {
            var yname = data.years[yidx];
            var value = NaN;
            
            chart_target_country_idx.forEach(function(tcidx) {
                chart_source_country_idx.forEach(function(scidx) {
                    if(!isNaN(data.migration[tcidx][yidx][scidx])) {
                        if(isNaN(value)) {
                            value = 0;
                        }
                        value += data.migration[tcidx][yidx][scidx];
                    }
                });
            });
            
            if(!isNaN(value)) {
                if(migmax < value) {
                    migmax = value;
                }
                return {
                    year: yname,
                    value: value,
                    percentage: null
                }; 
            }
        });
        
        migdataset = migdataset.filter(function(elem) { // filter out non existing elements
            return !!elem;
        });

        migdataset.forEach(function(d, idx) { // normalize values to maximum
            d.percentage = (0 == migmax ? 0 : d.value/migmax);
        });
        
        if(0 < migdataset.length) {
            chartdata.push({
                name: "Migration",
                data: migdataset
            });
        }

        //update scales       
        yscale.domain([0,1]);

        const xscale = d3.scale.ordinal().domain(chart_years);
                
        var realvaluedata = [];
        if(null != chartdata && 0 < chartdata.length && null != uistate.real_value_chart) {
            realvaluedata = chartdata.filter(function(d) { return d.name == uistate.real_value_chart; });
        }
        if(null != realvaluedata && 0 < realvaluedata.length) { // use real values instead of normalized ones for real value selected chart
            var chartdatareal = realvaluedata[0].data;
            
            yscalereal.domain(d3.extent(chartdatareal.map(
                function(elem) {
                    return elem.value;
                }
            )));
            xscale.rangePoints([0, dim.width]);
        }
        else {
            xscale.rangePoints([0, dim.width + 50]);
        }

        const xaxis = d3.svg.axis().scale(xscale).orient('bottom');       

        const linecolorscale = d3.scale.ordinal()
            .domain(chartdata.map(function(elem) { return elem.name; }))
            .range([uidef.color.gdp, uidef.color.er, uidef.color.migration])            

        const linecharts = root.select('g.chart').selectAll('path').data(chartdata);
        const linecharts_enter = linecharts.enter().append('path');
        
        //draw charts
        linecharts.transition().duration(def.transition_duration)
            .attr('d', function(pd) {
                var patharr = [];
                var chartname = pd.name;
                pd.data.forEach(function(d, i) {
                    //push operation
                    patharr.push(i == 0 ? "M" : "L");
                    //push x coordinate                    
                    patharr.push(xscale(d.year));
                    //push y coordinate
                    if(chartname == uistate.real_value_chart) {
                        patharr.push(yscalereal(d.value));
                    }
                    else {
                        patharr.push(yscale(d.percentage));
                    }
                });                
                return patharr.join(" ");
            })
            .style('stroke', function(d,i) {
                var c = linecolorscale(d.name);
                return c;
            });

        linecharts.exit().remove();

        // draw data value circle marks 
        const dotcontainer = root.select('g.chart').selectAll('g.cornerdots').data(chartdata);
        const dotcontainer_enter = dotcontainer.enter().append('g')
            .classed('cornerdots', true)
            .attr('fill-opacity', '1')
            .style('fill', function(chartd) {
                return linecolorscale(chartd.name);
            });
        dotcontainer.exit().remove();

        const dots = dotcontainer.selectAll('circle').data(function(chartd) { 
            return chartd.data.map(function(d) {
                return {
                    year: d.year,
                    value: d.value,
                    percentage: d.percentage,
                    chartname: chartd.name
                }
            } );
        });
        const dots_enter = dots.enter().append('circle');
                
        dots.transition().duration(def.transition_duration)
            .attr('r', def.dotsize)
            .attr('cy', function(d, i) {
                if(d.chartname == uistate.real_value_chart) {
                    return yscalereal(d.value);
                }
                else {
                    return yscale(d.percentage);
                }                
            })
            .attr('cx', function(d, i) {
                return xscale(d.year);
            })             
            .style('fill', 'inherit');
        
        dots.append('title').text(function(val){
            return val.value;
        });

        dots.exit().remove(); 
        
        // draw plot axes
        root.select('g.xaxis').transition().duration(def.transition_duration).call(xaxis);
        root.select('g.yaxis').transition().duration(def.transition_duration).call(yaxis);
        if(null != uistate.real_value_chart && null != realvaluedata && 0 < realvaluedata.length) { 
            //if any real value chart is selected, show the corresponding real valued axis
            root.select('g.yaxisreal').transition().duration(def.transition_duration).call(yaxisreal)
                .style({
                    'stroke': linecolorscale(uistate.real_value_chart),
                    'fill': linecolorscale(uistate.real_value_chart)
                });
        }
        else {
            //if no real value chart is selected, fade out the real valued axis
            root.selectAll('g.yaxisreal > *').transition().duration(def.transition_duration).remove();
        }

        root.select('g.axis').moveToFront();        
    }
    
    return update;
}

function createMigrationTopList(migrationtoplist_id) {
    const dim = {
        width: 600,
        height: 305,
        margin_top: 20,
        margin_bottom: 25,
        margin_left: 50
    };

    const def = {
        minbar_height_rat: 0.8,
        maxbar_height_rat: 0.1,
        transition_duration: 1000,
        toplist_size: 15,
        height: {
            normal: 305,
            expansion: 713
        }
    };

    const toplist_elem = d3.select(migrationtoplist_id);
     
    function update() {
        if(!uistate.loaded.finished()) {
            return;
        }

        //increase chart height if migration-gdp-er chart is not displayed
        if(uistate.year_start == uistate.year_end && false == uistate.year_selection) {            
            dim.height = def.height.expansion;
        }
        else {
            dim.height = def.height.normal;
        }

        //add drawing arra
        const svg = toplist_elem.selectAll('svg').data([1]);
        svg.enter().append('svg');
        
        svg.transition().duration(def.transition_duration)
            .attr('width', dim.margin_left + dim.width)
            .attr('height', dim.height + dim.margin_bottom + dim.margin_top)
        ;
        
        svg.exit().remove();
        
        const root = svg.selectAll('g.rootg').data([1])
        root.enter().append('g').classed('rootg', true).attr('transform','translate(' + dim.margin_left + ',' + dim.margin_top + ')');
        root.exit().remove();

        const unselector = root.selectAll('rect.unselector').data([1]) //add backgound for migration source country deselection
        unselector.enter().append('rect')
            .attr('width', dim.width)
            .classed('unselector', true)
            .attr('fill-opacity', 0)
            .on({
                click:  function() {
                            uistate.source_country = null;
                            updateUI();
                        }
            });
        unselector.attr('height', dim.height);
        unselector.exit().remove();
     
        // add axes containers
        const gxaxis = root.selectAll('g.xaxis').data([1]);
        gxaxis.enter().append('g').attr('class', 'axis xaxis')
            .attr('transform', 'translate(0,' + dim.height + ')');
        gxaxis.transition().duration(def.transition_duration)
            
        gxaxis.exit().remove();
        
        const gyaxis = root.selectAll('g.yaxis').data([1])
        gyaxis.enter().append('g').attr('class', 'axis yaxis');
        gyaxis.exit().remove();

        const gchart = root.selectAll('g.chart').data([1])
        gchart.enter().append('g').attr('class', 'chart');
        gchart.exit().remove();

        
        // add scales
        const xscale = d3.scale.ordinal().rangeRoundBands([0, dim.width], 0.1);
        const yscale = d3.scale.log().range([dim.height,0]);
        const xaxis = d3.svg.axis().scale(xscale).orient('bottom');
        const yaxis = d3.svg.axis().scale(yscale).orient('left'); 
        
        xaxis.outerTickSize(0);
        yaxis.outerTickSize(0);

        //Assemble data    
        var chart_years = data.years.filter(function(y) {            
            var chart_year_start = (null == uistate.year_start ? d3.extent(data.years)[0] : uistate.year_start);
            var chart_year_end = (null == uistate.year_end ? d3.extent(data.years)[1] : uistate.year_end);

            return (chart_year_start <= y &&  y <= chart_year_end);            
        });
        var chart_year_idx = chart_years.map( function(elem) {
            return data.years.indexOf(elem);
        });

        var chart_target_countries = [uistate.target_country];
        if(null == uistate.target_country) { // if no target country is selected, use all possible target countries
            chart_target_countries = data.target_countries;
        }
        var chart_target_country_idx = chart_target_countries.map(function(d) {
            return data.target_countries.indexOf(d);
        });

        // assemble source country top list by summing the migration values over the selected time interval
        // for the selected target countries
        var toplist = data.source_countries
            .map(function(scname, scidx) {
                var value = NaN;
            
                chart_target_country_idx.forEach(function(tcidx) {
                        chart_year_idx.forEach(function(yidx) {
                            if(!isNaN(data.migration[tcidx][yidx][scidx])) {
                                if(isNaN(value)) {
                                    value = 0;
                                }
                                value += data.migration[tcidx][yidx][scidx];
                            }
                        });  
                    });
                
                if(!isNaN(value)) {
                    return {
                        country: scname,
                        value: value
                    };
                }
            })
        toplist = toplist.filter(function(elem) { //sort the list of possible source countriesd descending, and use the 15 first elements
                return !!elem;
            })
            .sort(function(a,b) {
                return d3.descending(a.value, b.value);
            })
            .filter(function(elem, idx){                
                return (0 <= idx && def.toplist_size > idx) && 0 < elem.value;
            });
        
        //Update scales to current top list
        if(0 < toplist.length) {
            xscale.domain(toplist.map(  function(d) {
                                            return d.country;
                                        }));
            
            const ext = d3.extent(toplist,  function(d) {
                                                return d.value;  
                                            });
                                            
            ext[0] = ext[0] - (ext[0]*def.minbar_height_rat);
            ext[1] = ext[1] + (ext[1]*def.maxbar_height_rat)
            
            yscale.domain(ext);                     
        }
        //Add data bars
        const bars = root.select('g.chart').selectAll('rect').data(toplist, function(d) {return d.country;});
        const bars_enter = bars.enter().append('rect')
            .on({
                click:  function(d) {
                            uistate.source_country = d.country;
                            updateUI();
                        }
            });

        bars_enter.append('title')
            .text(function(d) {
                return d.value;
            });
        
        bars.transition().duration(def.transition_duration) // add on click selection
            .attr({
                x: function(d) {
                        scalectry = xscale(d.country); 
                        return scalectry; 
                    },
                y: function(d) { return yscale(d.value); },
                height: function(d) { return dim.height - yscale(d.value); },
                width: xscale.rangeBand()
            });

        bars.classed('selected',function(d) { // change bar class to highlight selection
                                    return d.country == uistate.source_country;
                                });
        bars.exit().remove(); 
        
        // add chart axes
        root.select('g.xaxis').transition().duration(def.transition_duration)
            .attr('transform', 'translate(0,' + dim.height + ')')
            .call(xaxis);
        root.select('g.yaxis').transition().duration(def.transition_duration).call(yaxis);
        root.select('g.axis').moveToFront();
    }

    return update;
}

function createGeomap(geomap_id) {
    const dim = {
        width: 450,
        height: 600,
    };
    
    const geomap = d3.select(geomap_id);
    
    const svg = geomap.append('svg')
        .attr({
            height: dim.height,
            width: dim.width          
        })
        .on({
            mouseleave: function(x) {
                            uistate.hover_country = null; // remove mouse hover hightlight if pointer leaves element
                        }
        });

    svg.append('rect') // add background deselection of the migration target country
        .attr('width', dim.width)
        .attr('height', dim.height)
        .attr('fill-opacity', 0)
        .on({
            click:  function() {
                        uistate.target_country = null;
                        updateUI();
                    }
        });
        
    const country_container = svg.append('g');
    
    const proj_scale = d3.scale.log().range([0, dim.width]);

    const projection = d3.geo.mercator()
        .scale(500) 
        .translate([dim.width*0.35,dim.height*1.5])
        .center([0,0])
        .precision(0.1);

    const path = d3.geo.path().projection(projection);
     
    function update() {
         // do not show or update the geomap if the parallel axes plot is selected for display
        if(true != uistate.visibility[0]) {
            geomap.style('display','none');    
            return;
        }
        else {
            geomap.style('display','block');
        }
        
        // do not show if data loading has not been finished
        if(!uistate.loaded.finished()) {
            return;
        }
        
        // add visual data paths
        const countries = country_container.selectAll('path')
            .data(data.geodata);
        const countries_enter = countries.enter().append('path')
            .attr('ISO3166',    function(x) {
                                    return x.properties.iso_a3;
                                })
            .attr('d', path)
            .on({ // add target country hovering and selection
                mouseenter: function(x) {
                                uistate.hover_country = x.properties.iso_a3;
                                updateUI();
                            },
                mouseleave: function(x) {
                                uistate.hover_country = null;
                                updateUI();
                            },
                click:      function(x) {
                                uistate.target_country = x.properties.iso_a3;
                                updateUI();
                            }
            });

        countries_enter.append('title')
            .text(  function(x) {
                        return x.properties.name;
                    });
        
        // change country path classes to highlight hovering and selection
        countries.classed('hovering',   function(d) {
                                            return d.properties.iso_a3 != uistate.target_country && d.properties.iso_a3 == uistate.hover_country;
                                        });
                                                
        countries.classed('selected',   function(d) { 
                                            return d.properties.iso_a3 == uistate.target_country; 
                                        });
        countries.exit().remove(); 
    }

    return update
}

function createGeolist(geolist_id) {
    const geolist = d3.select(geolist_id);
    
    const list = geolist.append('ul');
    
    function update() {        
        // do not show if data loading has not been finished
        if(!uistate.loaded.finished()) {
            return;
        }
        
        // append list items
        const items = list.selectAll('li').data(data.target_countries);
        const items_enter = items.enter().append('li')
            .on({
                click:  function(x) {
                            uistate.target_country = x;
                            updateUI();
                        },
                mouseenter: function(x) {
                                uistate.hover_country = x;
                                updateUI();
                            },
                mouseleave: function(x) {
                                uistate.hover_country = null;
                                updateUI();
                            }
            });

        items_enter.append('text')
            .text(  function(x) { // append text names from the geographical topojson data if possible
                        var cidx = geodata.countries.indexOf(x);
                        return (0 <= cidx && 0 < geodata.data.length) ? geodata.data[cidx].properties.name : x;
                    });

        // change list entries classes to highlight hovering and selection of migration target countries
        items.classed('hovering',  function(d) { 
                                        return d != uistate.target_country && d == uistate.hover_country; 
                                    });
        items.classed('selected',  function(d) { 
                                        return d == uistate.target_country; 
                                    });
        items.exit().remove();
        
        // correct floating box behaviour
        const clear = geolist.selectAll('.clear').data([1]);
        clear.enter().append('div')
            .attr('class', 'clear')
        clear.exit().remove();        
    }
    
    return update;
}

function createParallelAxis(countryaxis_id) {
    const dim = {
        width: 441,
        height: 585,
        padding_top: 10,
        padding_bottom: 5,
        padding_right: 5,
        padding_left: 4
    };
    
    const def = {
        transition_duration: 1000,
        dotsize: 4 //data point circle mark size
    };
    
    const root_elem = d3.select(countryaxis_id);
    
    const svg = d3.select(countryaxis_id).append('svg')
        .attr({            
            width: dim.width + dim.padding_right + dim.padding_left,
            height: dim.height + dim.padding_top + dim.padding_bottom
        });
    
    svg.append('rect') // background rectangle for target country deselection
        .attr('width', dim.width + dim.padding_right + dim.padding_left)
        .attr('height', dim.height + dim.padding_top + dim.padding_bottom)
        .attr('fill-opacity', 0)
        .on({
            click:  function() {
                        uistate.target_country = null;
                        updateUI();
                    }
        });
        
    const list_root = svg.append('g')
        .classed('list', true)
        .attr('transform', 'translate(0,' + dim.padding_top + ')');
    const chart_root = svg.append('g')
        .classed('chart', true)
        .attr('transform', 'translate('+ dim.padding_right +',' + dim.padding_top + ')');
        
    chart_root.append('g').attr('class', 'axis gdpaxis')
        .attr('transform', 'translate(0,0)');
    chart_root.append('g').attr('class', 'axis eraxis')
        .attr('transform', 'translate(' + dim.width/2 + ',0)');
    chart_root.append('g').attr('class', 'axis migaxis')
        .attr('transform', 'translate(' + dim.width + ',0)');    
    
    const line_root = chart_root.append('g')
        .classed('lines', true);

    const scalesidxnames = [
        'gdp','er','mig'  
    ]; 
    const scales = {
        gdp: d3.scale.log().range([dim.height, 0]),
        er: d3.scale.log().range([dim.height, 0]),
        mig: d3.scale.log().range([dim.height, 0])
    };
        
    const gdp_scale = d3.scale.log().range([dim.height, 0]);
    const er_scale = d3.scale.log().range([dim.height, 0]);
    const mig_scale = d3.scale.log().range([dim.height, 0]);
    
    const gdp_axis = d3.svg.axis().scale(scales['gdp']).orient('right')
        .tickFormat(d3.format(".2e"));
    const er_axis = d3.svg.axis().scale(scales['er']).orient('left')
        .tickFormat(d3.format(".2e"));
    const mig_axis = d3.svg.axis().scale(scales['mig']).orient('left')
        .tickFormat(d3.format(".2e"));
        
    function update() {
        // only update and show this plot if it is selected for display
        if(true != uistate.visibility[1]) {
            root_elem.style('display','none');
            return;
        }
        else {
            root_elem.style('display','block');
        }
        
        // get year selection data
        const chart_years = data.years.filter(function(y) {
            var chart_year_start = (null == uistate.year_start ? d3.extent(data.years)[0] : uistate.year_start);
            var chart_year_end = (null == uistate.year_end ? d3.extent(data.years)[1] : uistate.year_end);

            return (chart_year_start <= y &&  y <= chart_year_end);
        });
        const chart_year_idx = chart_years.map(function(d) {
            return data.years.indexOf(d);
        });
                
        // get source country selection data
        var chart_source_countries = [uistate.source_country];
        if(null == uistate.source_country) { // if no source country has been selected, use all source countries
            chart_source_countries = data.source_countries;
        }
        var chart_source_country_idx = chart_source_countries.map( function(d) {
            return data.source_countries.indexOf(d);
        });
        
        // assemble gdp, employment rate data for each country for the selected time interval and
        // the selected migration source country
        const country_data = data.target_countries.map(function(tcname, tcidx) {
            var gdp_data = NaN;
            var er_data = NaN;
            var mig_data = NaN;
            
            chart_year_idx.forEach(function(yidx) {
                if(!isNaN(data.gdp[tcidx][yidx])) {
                    if(isNaN(gdp_data)) {
                        gdp_data = 0;
                    }
                    gdp_data += data.gdp[tcidx][yidx]; 
                }
                if(!isNaN(data.er[tcidx][yidx])) {
                    if(isNaN(er_data)) {
                        er_data = 0;
                    }
                    er_data += data.er[tcidx][yidx];
                }
                chart_source_country_idx.forEach(function(scidx) {
                    if(isNaN(mig_data)) {
                        mig_data = 0;
                    }
                    if(!isNaN(data.migration[tcidx][yidx][scidx])) {
                        mig_data += data.migration[tcidx][yidx][scidx];
                    }
                });                
            });
            
            return{
                country_name: tcname,
                gdp: gdp_data,
                er: er_data,
                mig: mig_data
            };
        });        
        
        // update scale domains
        var gdpext = d3.extent(country_data.filter(
                        function(elem){ 
                            return !isNaN(elem.gdp) && 0 != elem.gdp; 
                        })
                        .map(function(elem) {
                            return elem.gdp;
                        }));
        var erext = d3.extent(country_data.filter(
                        function(elem){ 
                            return !isNaN(elem.er) && 0 != elem.er;
                        })
                        .map(function(elem) {
                            return elem.er;
                        }));
        var migext = d3.extent(country_data.filter(
                        function(elem){ 
                            return !isNaN(elem.mig) && 0 != elem.mig; 
                        })
                        .map(function(elem) {
                            return elem.mig;
                        }));
        
        scales['gdp'].domain(gdpext);
        scales['er'].domain(erext);
        scales['mig'].domain(migext);
        
        // draw chart
        const charts = line_root.selectAll('path').data(country_data);
        const charts_enter = charts.enter().append('path');
        charts.transition().duration(def.transition_duration)
            .attr('d', function(elem) {
                var trail = [];
                ['gdp', 'er', 'mig'].forEach(function(dimension, dim_idx) {
                    var value = elem[dimension];
                    if(isNaN(value)) {
                        return;
                    }
                    var scaled = scales[dimension](value);
                    if(isNaN(scaled)) {
                        return;
                    }
                    trail.push(0 < trail.length ? 'L': 'M');
                    trail.push(dim_idx * dim.width/2);
                    trail.push(scaled);
                })                           
                return trail.join(" ");
            })
            .attr('stroke-linecap', 'butt');
            
        charts.on({ // allow migration target country selection on click and highlighting on mouse hover
                click:  function(elem) {
                            uistate.target_country = elem.country_name;
                            updateUI();
                        },
                mouseenter: function(elem) {
                                uistate.hover_country = elem.country_name;
                                updateUI();
                            },
                mouseleave: function(elem) {
                                uistate.hover_country = null;
                                updateUI();
                            }
            });
        
        
        // change country path classes to highlight hovering and selection
        charts.classed('hovering', function(elem){
            return elem.country_name != uistate.target_country && elem.country_name == uistate.hover_country;
        });
            
        charts.classed('selected', function(elem){
            return elem.country_name == uistate.target_country;
        });
        
        charts.exit().remove();
        
        // add points at axis intersections if data values exist
        const dotcontainers = line_root.selectAll('g.dotcontainer').data(country_data);
        dotcontainers.enter().append('g').classed('dotcontainer', true);
        
        // change data value mark container classes to highlight hovering and selection
        dotcontainers.classed('hovering', function(elem){
            return elem.country_name != uistate.target_country && elem.country_name == uistate.hover_country;
        });
            
        dotcontainers.classed('selected', function(elem){
            return elem.country_name == uistate.target_country;
        });
        
        // add marks to the containers
        const dots = dotcontainers.selectAll('circle').data(function(elem) { // chain data to the mark elements from the mark container of each country
            var retval = [];
            ['gdp','er','mig'].forEach(function(dim, dimidx) {
                if(!isNaN(elem[dim]) && 0 < elem[dim]) {
                    retval.push({
                        country_name: elem.country_name,
                        dim: dim,
                        axisnr: dimidx,
                        value: elem[dim]
                    });
                }
            });
            return retval;
        });
        dots.enter().append('circle')
            .attr('r', def.dotsize);

        dots.transition().duration(def.transition_duration)
                .attr('cy', function(d, i) {
                    var val = scales[d.dim](d.value);                    
                    return val;
                })
                .attr('cx', function(d, i) {
                    return d.axisnr * dim.width/2;
                });
        dots.on({ // allow migration target country selection on click and highlighting on mouse hover
                click:  function(elem) {
                            uistate.target_country = elem.country_name;
                            updateUI();
                        },
                mouseenter: function(elem) {
                                uistate.hover_country = elem.country_name;
                                updateUI();
                            },
                mouseleave: function(elem) {
                                uistate.hover_country = null;
                                updateUI();
                            }
            });
       
        dots.append('title').text(function(elem) { return elem.value; });

        dots.exit().remove();

        dotcontainers.exit().remove();

        // draw axes        
        chart_root.select('g.gdpaxis').transition().duration(def.transition_duration).call(gdp_axis)
        chart_root.select('g.eraxis').transition().duration(def.transition_duration).call(er_axis);
        chart_root.select('g.migaxis').transition().duration(def.transition_duration).call(mig_axis);
    }
    
    return update;
}

function createTimeline(timeline_id) {
    const dim = {
        width: 800,
        height: 50,
        padding_left: 10,
        padding_right: 10,
        padding_top: 6       
    };     
    
    const tldef = {
        limiter_r: 4,
        trans_speed: 60
    };
    
    const svg = d3.select(timeline_id).append('svg').attr({
            width: dim.width + dim.padding_left + dim.padding_right,
            height: dim.height
        })
        .on({
            mouseleave: function(d) {
                uistate.year_selection = false;
                updateUI();
            }
        });

    
    svg.append('path').attr({
        class: 'tlaxis',
        d: [
            'M',0 ,dim.height/2 + 10 + dim.padding_top, 
            'l',dim.width + dim.padding_left + dim.padding_right,0
           ].join(" ")
    });        
    
    //Add some container for later    
    const ynumber_container = svg.append('g').attr('class', 'ynumber_container');    
    const vline_container = svg.append('g').attr('class', 'vline_container');
    const highlight_container = svg.append('g').attr('class', 'highlight_container');    
    const ticks_container = svg.append('g').attr('class', 'ticks_container');
    
    function update() {    
        //Display the year numbers
        const ynumbers = ynumber_container.selectAll('text').data(data.years);        
        ynumbers.enter().append('text')
            .attr('text-anchor', 'middle')
            .attr('x',  function(d,i) {
                            var yidx = d - data.years.min();
                            var offset = dim.width/(2*data.years.length);
                            return dim.padding_left + offset + yidx * dim.width/data.years.length;
                        }
                )
            .attr('y',  function(d,i) {
                            var yidx = d - data.years.min();
                            var offset_south = 17;//25;
                            var offset_north = 17;//12;
                            return (0 == yidx % 2 ? offset_north : offset_south) + dim.padding_top;
                        })
            .text(  function(d) {
                        var str_d = d + "";
                        return str_d.substring(0,4); 
                    });    
        
        ynumbers.classed('selected',function(d) { 
                                        return (uistate.year_start <= d &&  uistate.year_end >= d); 
                                    });
                                    
        ynumbers.exit().remove();   

        //Display connectors between numbers and the time line
        const vlines = vline_container.selectAll('path').data(data.years);        
        var vlines_enter = vlines.enter().append('path')
            .attr('d',  function(d,i) {
                            var yidx = d - data.years.min();
                            var param = ['M',0,dim.height/2 + 10.5 + dim.padding_top,'l',0,0];
                            var loffset = dim.padding_left + dim.width / (2*data.years.length);
                            param[1] = loffset + yidx * dim.width / data.years.length;                
                            param[5] = -13;//-(0 == yidx % 2 ? 20 : 7 );
                            return param.join(" ");
                        });
        vlines.classed('selected',  function(d) { 
                                        return (uistate.year_start <= d &&  uistate.year_end >= d); 
                                    });
                                    
        vlines.exit().remove();
        
        //Add highlighting of selected years and of the timeline between these years        
        var highlights = highlight_container.selectAll('rect').data([1]);
        highlights.enter().append('rect');
        highlights.transition().duration(tldef.trans_speed)
            .attr('y', dim.height/2 + 8.75 + dim.padding_top)
            .attr('x',  function(d,i) {
                            var sy_idx = uistate.year_start - data.years.min();
                            var loffset = dim.padding_left + dim.width / (2*data.years.length);
                            return loffset + sy_idx * dim.width / data.years.length + 1;   
                        })
            .attr('width', function(d,i) {
                            
                            var sy_idx = uistate.year_start - data.years.min();
                            var ey_idx = uistate.year_end - data.years.min();
                            var y_idx_diff = ey_idx - sy_idx;
                            return y_idx_diff * dim.width/data.years.length;
                        })
            .attr('height', 2.6);
        highlights.exit().remove();
        
        // add circles at the axis hightlight start and ends
        var limiters = highlight_container.selectAll('circle').data(['s','e']);
        limiters.enter().append('circle');
        limiters.transition().duration(tldef.trans_speed)
            .attr('r', tldef.limiter_r)
            .attr('cy', dim.height/2 + 10 + dim.padding_top)
            .attr('cx', function(d,i) {                            
                            var y_idx = (d == 's' ? uistate.year_start : uistate.year_end) - data.years.min();                         
                            var loffset = dim.padding_left + dim.width / (2*data.years.length);
                            return loffset + y_idx * dim.width / data.years.length; 
                        })        
        limiters.exit().remove();   
            
        //Add selection areas for the individual ticks
        const ticks = ticks_container.selectAll('rect').data(data.years);        
        const ticks_enter = ticks.enter().append('rect')
            .attr('width', dim.width/data.years.length)
            .attr('height', dim.height)
            .attr('x',  function(d,i) { 
                            var yidx = d - data.years.min();
                            return dim.padding_left + yidx * dim.width / data.years.length
                        })
            .attr('y', 0)
            .on({
                mousedown:  function(d) {
                                uistate.year_end = d;
                                uistate.year_start = d;
                                uistate.year_selection = true;
                                updateUI();
                            },
                mouseup:    function(d) {
                                uistate.year_selection = false;
                                updateUI();
                            }, 
                mouseenter: function(d) {
                                if(uistate.year_selection) {
                                    if(d < uistate.year_start) {
                                        uistate.year_start = d;
                                    }
                                    else {
                                        uistate.year_end = d;
                                    }
                                    updateUI();
                                }
                            }
            });    
        ticks_enter.append('title'); //tooltip
        ticks.select('title').text(function(d) { return d; });
        
        ticks.exit().remove();
    }
    
    return update;    
}
